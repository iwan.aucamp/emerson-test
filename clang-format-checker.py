#!/usr/bin/env python3

import logging
import sys
import argparse
import fnmatch
import os
import re
import subprocess

def main():
    logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

    argument_parser = argparse.ArgumentParser(add_help = False)
    argument_parser.add_argument("-v", "--verbose", action="count", dest="verbosity", help="increase verbosity level")
    argument_parser.add_argument("-h", "--help", action="help", help="shows this help message and exit")
    argument_parser.add_argument("-i", "--ignore", dest="ignores", action="append", default=[], type=str, help="regex to ignore")
    argument_parser.add_argument("paths", action="store", nargs='+', help="paths to check")
    arguments = argument_parser.parse_args( args = sys.argv[1:] )

    if arguments.verbosity is not None:
        root_logger = logging.getLogger("")
        new_level = ( root_logger.getEffectiveLevel() - (min(1,arguments.verbosity))*10 - min(max(0,arguments.verbosity - 1),9)*1 )
        root_logger.setLevel( new_level )
        root_logger.propagate = True

    logging.debug("sys.argv = %s, arguments = %s, logging.level = %s", sys.argv, arguments, logging.getLogger("").getEffectiveLevel())

    cfiles = []

    for path in arguments.paths:
        for root, dirnames, filenames in os.walk(os.path.abspath(path)):
            logging.debug("root = %s", root)
            for filename in filenames:
                if re.match(r"^.*[.](h|c)(xx|pp|)$", filename):
                    cfiles.append( os.path.join( root, filename ) )

    logging.debug("cfiles = %s", cfiles)

    for cfile in cfiles:
        logging.debug("vimdiff '{0:}' <(clang-format -style=file -fallback-style=none '{0:}')".format( cfile ))
        skip = False
        for ignore in arguments.ignores:
            if re.match( ignore, cfile ):
                logging.info("skipping %s because of ignore %s", cfile, ignore)
                skip = True
                break;
        if skip:
            logging.info("skipping %s", cfile)
            break;
        with subprocess.Popen(["clang-format", "-style=file", "-fallback-style=none", cfile], stdout=subprocess.PIPE) as fproc:
            logging.debug("fproc.stdout.fileno() = %s", fproc.stdout.fileno())
            #logging.debug("proc.stdout = %s", proc.stdout.read())
            with subprocess.Popen(["diff", "--color=auto", "-u", cfile, "-"], stdin=fproc.stdout) as dproc:
            #with subprocess.Popen(["diff", "--color=auto", "-u", cfile, "/proc/{}/fd/{}".format(fproc.pid, fproc.stdout.fileno())]) as dproc:
                logging.debug("running ...")


if __name__ == "__main__":
    main()
