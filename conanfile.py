# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html
#

import conans
import os
import logging
import sys
logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

class MyConan(conans.ConanFile):
    name = "iwana-template-cxx"
    version = "1.0.0-r0"
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        #"Poco/1.9.0@pocoproject/stable",
        "boost/1.69.0@conan/stable",
        #"qt/5.11.3@bincrafters/stable",
    )

    exports_sources = ( "*", "!build/*" )
    generators = (
        "scons",
        "cmake",
        "cmake_multi",
        "cmake_paths",
        "cmake_find_package",
        "txt",
        "virtualenv",
        "virtualbuildenv",
        "virtualrunenv",
        "gcc",
        "compiler_args",
        "make",
        "json",
        "pkg_config",
    )
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "builder": ["cmake", "scons"]
    }
    default_options = {
        "builder": "scons",
        "shared": False,
        "fPIC": True,
        "Poco:enable_crypto": False,
        "Poco:enable_data_sqlite": False,
        "Poco:enable_mongodb": False,
        "Poco:enable_netssl": False,
        "Poco:enable_netssl_win": False,
        "Poco:enable_util": False,
        "Poco:enable_xml": False,
        "Poco:enable_zip": False,
        "Poco:force_openssl": False,
    }

    def build(self):
        logging.info("self.options.builder = %s", self.options.builder)
        logging.info("self.source_folder = %s", self.source_folder)
        logging.info("self.build_folder = %s", self.build_folder)
        logging.info("os.getcwd() = %s", os.getcwd())
        if self.options.builder == "cmake":
            self.build_cmake()
        elif self.options.builder == "scons":
            self.build_scons()
        else:
            raise RuntimeError("Invalid builder {}".format(self.options.builder))

    def build_cmake(self):
        cmake = conans.CMake(self)
        cmake.verbose = True
        cmake.configure()
        cmake.build()
        with conans.tools.environment_append({"BOOST_TEST_LOG_LEVEL": "all", "ARGS": "--verbose"}):
            cmake.test()

    def build_scons(self):
        debug_opt = '--debug-build' if self.settings.build_type == 'Debug' else ''
        with conans.tools.chdir(self.build_folder):
            self.run('scons -C {} {}'.format(self.source_folder, debug_opt))
        #self.run('scons {}'.format(debug_opt))
