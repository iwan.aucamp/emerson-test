#include "debug.hpp"

namespace debug {
bool big_endian = is_big_endian();
std::atomic_int function_boundry_helper::m_indent(0);
std::function<void(int, const char*, size_t length)> writer = &_writer;
} // namespace debug
