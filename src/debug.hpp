#pragma once
#include <arpa/inet.h>
#include <atomic>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <typeinfo>
#include <unistd.h>
#include <unordered_map>
#include <utility>
#include <vector>

// and now !?

namespace debug {

template <typename ValueType>
class xdump
{
public:
    const ValueType& _value;
    explicit xdump(const ValueType& value)
        : _value(value) {}
};

template <typename ValueType>
xdump<ValueType> mxdump(const ValueType& value) {
    return xdump<ValueType>(value);
}

inline bool is_big_endian() {
    return htonl(0xAF) == 0xAF;
}

//bool big_endian = is_big_endian();
extern bool big_endian;

template <typename KeyType, typename MappedType>
std::ostream& operator<<(std::ostream& output, const std::map<KeyType, MappedType>& input);
template <typename KeyType, typename MappedType, typename HasherType>
std::ostream& operator<<(std::ostream& output, const std::unordered_map<KeyType, MappedType, HasherType>& input);
template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::vector<ValueType>& input);
template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::set<ValueType>& input);
template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::unique_ptr<ValueType>& input);

template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const xdump<ValueType>& input) {
    output << input._value;
    //debug::operator<<(output, input._value);
    return output;
}

template <typename KeyType, typename MappedType>
std::ostream& operator<<(std::ostream& output, const std::map<KeyType, MappedType>& input) {
    //using MapType = std::map< KeyType, MappedType >;
    bool first = true;
    output << "{ ";
    for (auto cit = input.begin(); cit != input.end(); ++cit) {
        if (first) {
            first = false;
        }
        else
            output << ", ";
        output << cit->first << ": " << cit->second; // XXX TODO FIXME
    }
    output << " }";
    return output;
}

template <typename KeyType, typename MappedType, typename HasherType>
std::ostream& operator<<(std::ostream& output, const std::unordered_map<KeyType, MappedType, HasherType>& input) {
    //using MapType = std::unordered_map< KeyType, MappedType, HasherType >;
    bool first = true;
    output << "{ ";
    for (auto cit = input.begin(); cit != input.end(); ++cit) {
        if (first) {
            first = false;
        }
        else
            output << ", ";
        output << cit->first << ": " << cit->second; // XXX TODO FIXME
    }
    output << " }";
    return output;
}

template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::vector<ValueType>& input) {
    //using VectorType = std::vector< ValueType >;
    bool first = true;
    output << "[ ";
    for (auto cit = input.begin(); cit != input.end(); ++cit) {
        if (first) {
            first = false;
        }
        else
            output << ", ";
        output << *cit;
    }
    output << " ]";
    return output;
}

template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::set<ValueType>& input) {
    //using VectorType = std::set< ValueType >;
    bool first = true;
    output << "[ ";
    for (auto cit = input.begin(); cit != input.end(); ++cit) {
        if (first) {
            first = false;
        }
        else
            output << ", ";
        output << *cit;
    }
    output << " ]";
    return output;
}

/*
template< typename CharType >
std::ostream& operator <<(std::ostream& output, const std::vector< std::basic_string<CharType> >& input)
{
    using VectorType = std::vector< std::basic_string< CharType > >;
    bool first = true;
    output << "[";
    for (typename VectorType::const_iterator cit = input.begin(); cit != input.end(); ++cit)
    {
        if (first)
        {
            first = false;
            output << std::endl;
        }
        else output << "," << std::endl;
        output << "  " << *cit; // XXX TODO FIXME
    }
    if (!first) output << std::endl;
    output << "]";
    return output;
}
*/

template <typename A, typename B>
std::ostream& operator<<(std::ostream& output, const std::pair<A, B>& input) {
    output << "(" << input.first << ", " << input.second << ")";
    return output;
}

template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const std::unique_ptr<ValueType>& input) {
    if (input)
        output << *input;
    else
        output << "NULL";
    return output;
}

inline std::string hexdump(const char* base, size_t size) {
    std::stringstream out;
    out << "[ ";
    out << std::hex << std::setfill('0');
    bool first = true;
    for (size_t i = 0; i < size; ++i) {
        if (first)
            first = false;
        else
            out << ", ";
        out << "0x" << std::setw(2) << static_cast<int>(base[i]);
    }
    out << " ]";
    return out.str();
}

template <typename NumberType>
std::string hexdump(NumberType number) {
    return hexdump(static_cast<const char*>(&number), sizeof(NumberType));
}

template <typename T>
std::string stringify(const T& in) {
    std::stringstream out;
    out << in;
    return out.str();
}

template <typename ContainerType>
class csh
{
public:
    const ContainerType& container;

    csh(const ContainerType& container)
        : container(container) {
    }

    void dump(std::ostream& out) const {
        out << "{ ";
        bool first = true;
        for (typename ContainerType::const_iterator it = this->container.begin(); it != this->container.end(); ++it) {
            if (first)
                first = false;
            else
                out << ", ";
            out << *it;
        }
        out << " }";
    }
};

template <typename ContainerType>
const csh<ContainerType> cshm(ContainerType& container) {
    return csh<ContainerType>(container);
}

template <typename ContainerType>
std::ostream& operator<<(std::ostream& out, const csh<ContainerType>& c) {
    c.dump(out);
    return out;
}

/*
void _writer(const std::string& string)
{
    write(fileno(stderr), string.c_str(), string.size());
}

std::function< void(const std::string&) > writer = &_writer;
*/

inline void _writer(int fd, const char* message, size_t length) {
    write(fd, message, length);
}

extern std::function<void(int, const char*, size_t length)> writer;
//boost::function< void(int, const char*, size_t length) > writer = &_writer;

inline std::string nowstring() {

    struct timespec l_timespec;
    clock_gettime(CLOCK_REALTIME, &l_timespec);

    struct tm l_tm;
    localtime_r(&(l_timespec.tv_sec), &l_tm);

    char date_time_buf[32];
    strftime(date_time_buf, 32, "%Y-%m-%dT%H:%M:%S", &l_tm);

    char rbuf[64];
    int written = snprintf(rbuf, 64, "%s.%09li", date_time_buf, l_timespec.tv_nsec);
    (void)written;
    return std::string(rbuf);
}

class function_boundry_helper
{
public:
    using boundry = std::function<void(const std::ostream&)>;

private:
    std::string m_name;
    boundry m_return;
    static std::atomic_int m_indent;

public:
    function_boundry_helper(std::string name, const boundry& entry = boundry(), boundry return_ = boundry())
        : m_name(std::move(name))
        , m_return(std::move(return_)) {
        for (int i = 0; i < function_boundry_helper::m_indent; ++i) std::cerr << "  ";
        function_boundry_helper::m_indent++;

        std::cerr << nowstring() << " : -> " << this->m_name << ":entry";
        if (entry) entry(std::cerr);
        std::cerr << std::endl
                  << std::endl;
    }

    ~function_boundry_helper() {
        std::cerr << std::endl;
        function_boundry_helper::m_indent--;
        for (int i = 0; i < function_boundry_helper::m_indent; ++i) std::cerr << "  ";

        std::cerr << nowstring() << " : <- " << this->m_name << ":return";
        if (this->m_return) this->m_return(std::cerr);
        std::cerr << std::endl;
    }
};

using fbh = function_boundry_helper;
} // namespace debug

#define DEBUG_VAR(var) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%Y-%m-%dT%H:%M:%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%09li [%08u:%08u:%010u] DEBUG_VAR [%s:%i:%s] [ (%s:%04lu) %s = %s ]\n", \
            date_time_buf, l_timespec.tv_nsec, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            "xxx", __LINE__, __FUNCTION__, \
            typeid(var).name(), sizeof(var), #var, debug::stringify((var)).c_str()); \
        debug::writer(fileno(stderr), __db_buffer, written); \
    }

#define DEBUG_VARFS(fspec, var) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%Y-%m-%dT%H:%M:%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%09li [%08u:%08u:%010u] DEBUG_VAR [%s:%i:%s] [ (%s:%04lu) %s = " fspec " ]\n", \
            date_time_buf, l_timespec.tv_nsec, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            "xxx", __LINE__, __FUNCTION__, \
            typeid(var).name(), sizeof(var), #var, var); \
        debug::writer(fileno(stderr), __db_buffer, written); \
    }

#define DEBUG_PRINTF(format, ...) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%Y-%m-%dT%H:%M:%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%09li [%08u:%08u:%010u] DEBUG_VAR [%s:%i:%s] " format "\n", \
            date_time_buf, l_timespec.tv_nsec, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            "xxx", __LINE__, __FUNCTION__, \
            __VA_ARGS__); \
        write(fileno(stderr), __db_buffer, written); \
    }
