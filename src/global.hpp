#pragma once

#include <boost/log/common.hpp>
#include <boost/log/trivial.hpp>

BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(g_logger, boost::log::sources::severity_logger_mt<boost::log::trivial::severity_level>)
