#include "debug.hpp"
#include "global.hpp"
#include "models.hpp"

#include <boost/log/trivial.hpp>

// https://www.boost.org/doc/libs/release/doc/html/program_options.html
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <boost/stacktrace.hpp>

#include <iostream>

#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup.hpp>

#include <yaml-cpp/yaml.h>

#include "enum.h"

BETTER_ENUM(cli_commands, unsigned int,
    dump_manifest,
    dump_layout,
    dump_attributes,
    assign_seats)


int main(int argc, char* argv[]) {
    //try {
    {
        //boost::log::add_console_log( std::clog, boost::log::keywords::format = "%TimeStamp% %ProcessID% %ThreadID% %Severity% %Message%" );a
        namespace bpo = boost::program_options;
        bpo::options_description generic_options("Allowed Options");

        generic_options.add_options() //
            ("help,h", "produce help message") //
            ("verbose,v", bpo::value<std::vector<bool> >()->zero_tokens(), "increase verbosity") //
            ;


        bpo::options_description hidden_options("Hidden Options");
        hidden_options.add_options() //
            ("command", bpo::value<std::string>(), "command to execute") //
            ("subargs", bpo::value<std::vector<std::string> >(), "Arguments for command") //
            ;

        bpo::options_description cmdline_options;
        cmdline_options.add(generic_options).add(hidden_options);

        bpo::positional_options_description pos;
        pos.add("command", 1).add("subargs", -1);

        bpo::variables_map variables_map;
        //auto parsed = bpo::parse_command_line( argc, argv, option_description );
        auto parsed = bpo::command_line_parser(argc, argv) //
                          .options(cmdline_options) //
                          .positional(pos) //
                          .allow_unregistered() //
                          .run();
        bpo::store(parsed, variables_map);
        bpo::notify(variables_map);
        int verbosity = std::count_if(parsed.options.begin(), parsed.options.end(),
            [](const boost::program_options::basic_option<char>& item) { return item.string_key == "verbose"; });

        if (variables_map.count("help") != 0u && variables_map.count("command") == 0u) {
            std::cerr << generic_options << std::endl;
            return EXIT_SUCCESS;
        }

        namespace logging = boost::log;
        namespace expr = boost::log::expressions;
        namespace keywords = boost::log::keywords;
        namespace attrs = boost::log::attributes;
        logging::add_console_log(std::clog,
            keywords::filter = expr::attr<logging::trivial::severity_level>("Severity") >= (logging::trivial::warning - std::min(3, verbosity)),
            keywords::format = expr::format("%1% %2% %3% %4% ::: %5%") //
                % expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%dT%H:%M:%S.%f") //
                % expr::attr<attrs::current_process_id::value_type>("ProcessID") //
                % expr::attr<attrs::current_thread_id::value_type>("ThreadID") //
                % expr::max_size_decor<char>(7)[expr::stream << std::setw(7) << logging::trivial::severity] //
                % expr::message //
        );
        logging::add_common_attributes();

        if (variables_map.count("command") == 0u) {
            std::cerr << "ERROR: no sub command found ..." << std::endl;
            std::cerr << generic_options << std::endl;
            return EXIT_SUCCESS;
        }

        auto do_sub_parse = [&](const bpo::options_description& suboption_description) -> boost::optional<int> {
            std::vector<std::string> opts = bpo::collect_unrecognized(parsed.options, bpo::include_positional);
            opts.erase(opts.begin());
            bpo::store(bpo::command_line_parser(opts).options(suboption_description).run(), variables_map);

            if (variables_map.count("help") != 0u) {
                std::cerr << generic_options << std::endl;
                std::cerr << suboption_description << std::endl;
                return EXIT_SUCCESS;
            }

            bpo::notify(variables_map);
            return boost::none;
        };

        auto command_string = variables_map["command"].as<std::string>();
        auto command_optional = cli_commands::_from_string_nothrow(command_string.c_str());
        if (!command_optional) throw std::runtime_error("Invalid command " + command_string);
        auto command = *command_optional;
        BOOST_LOG_SEV(g_logger::get(), boost::log::trivial::info) << "Started: command = " << command;
        if (command == (+cli_commands::dump_layout)) {
            bpo::options_description suboption_description("dump_layout options");
            suboption_description.add_options() //
                ("layout,l", bpo::value<std::string>()->required(), "seat layout file") //
                ;

            if (auto rv = do_sub_parse(suboption_description)) return *rv;

            auto layout = model::airplane::layout::from_file(variables_map.at("layout").as<std::string>());
            YAML::Emitter out;
            out << *layout;
            std::cout << out.c_str() << std::endl;
        }
        else if (command == (+cli_commands::dump_attributes)) {
            bpo::options_description suboption_description("dump_attributes options");
            suboption_description.add_options() //
                ("layout,l", bpo::value<std::string>()->required(), "seat layout file") //
                ;
            if (auto rv = do_sub_parse(suboption_description)) return *rv;
            auto layout = model::airplane::layout::from_file(variables_map.at("layout").as<std::string>());
            std::unique_ptr<model::attributes::layout> attributes(new model::attributes::layout(*layout));
            YAML::Emitter out;
            out << *attributes;
            std::cout << out.c_str() << std::endl;
        }
        else if (command == (+cli_commands::dump_manifest)) {
            bpo::options_description suboption_description("dump_manifest options");
            suboption_description.add_options() //
                ("manifest,m", bpo::value<std::string>()->required(), "passanger manifest") //
                ;

            if (auto rv = do_sub_parse(suboption_description)) return *rv;

            auto manifest = model::passanger::manifest::from_file(variables_map.at("manifest").as<std::string>());
            YAML::Emitter out;
            out << *manifest;
            std::cout << out.c_str() << std::endl;
        }
        else if (command == (+cli_commands::assign_seats)) {
            bpo::options_description suboption_description("assign_seats options");
            suboption_description.add_options() //
                ("layout,l", bpo::value<std::string>()->required(), "seat layout file") //
                ("manifest,m", bpo::value<std::string>()->required(), "passanger manifest") //
                ;

            if (auto rv = do_sub_parse(suboption_description)) return *rv;

            auto layout = model::airplane::layout::from_file(variables_map.at("layout").as<std::string>());
            auto manifest = model::passanger::manifest::from_file(variables_map.at("manifest").as<std::string>());
            std::unique_ptr<model::attributes::layout> attributes(new model::attributes::layout(*layout));
            auto assignments = model::assignments( *layout, *manifest, *attributes );

            YAML::Emitter out;
            out << assignments;
            std::cout << out.c_str() << std::endl;
        }
        else {
            throw std::runtime_error(std::string("Invalid command ") + command._to_string());
        }
        BOOST_LOG_SEV(g_logger::get(), boost::log::trivial::info) << "Completed succesfully = " << command;
        return EXIT_SUCCESS;
    }
    /*
    catch (const std::exception& exception) {
        std::cerr << "ERROR: " << exception.what() << std::endl;
        const boost::stacktrace::stacktrace* st = boost::get_error_info<traced>(exception);
        if (st != nullptr) {
            std::cerr << *st << std::endl;
        }
        else throw exception;
        return EXIT_FAILURE;
    }
    catch (...) {
        std::cerr << "ERROR: "
                  << "UNKNOWN" << std::endl;
        //std::cerr << boost::stacktrace::stacktrace() << std::endl;
        return EXIT_FAILURE;
    }
    */
}
