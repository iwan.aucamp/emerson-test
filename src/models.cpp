#include "models.hpp"
#include <boost/optional/optional_io.hpp>

namespace model {

namespace ascii {

std::vector<point> point::parse_line(const std::string& line) {
    std::vector<point> result;
    size_t index = 0;
    for (size_t position = 0; position < line.size(); ++position) {
        const auto& cp = line.at(position);
        if (!std::isspace(cp, std::locale("C"))) {
            result.emplace_back(cp, index++, position);
        }
    }
    return result;
}

point::point(char value, size_t index, size_t position)
    : value(value)
    , index(index)
    , position(position) {}

std::ostream& operator<<(std::ostream& output, const point& input) {
    output << boost::format("ascii::point{ value = %1%, index = %2%, position = %3% }") //
            % input.get_value() % input.get_index() % input.get_position();
    return output;
}

} // namespace ascii


namespace pattern {
std::regex normal = std::regex(R"(^\s*([|XZ=])\s*([0WSIGBLCPKB ]*?)\s*([|XZ=])\s*(#\s*(.*?)\s*|)$)", std::regex::ECMAScript);
std::regex indexing = std::regex(R"(^\s*[#]\s*(\S.+?)\s*[#]\s*$)", std::regex::ECMAScript);
std::regex meta = std::regex(R"(^\s*[%]\s*(\S.+?)\s*=\s*(\S*?)\s*$)", std::regex::ECMAScript);
std::regex blank = std::regex(R"(^\s*$)", std::regex::ECMAScript);
} // namespace pattern

seat_designation::seat_designation(unsigned int row, char column)
    : row(row)
    , column(column) {
}

seat_designation::seat_designation(const YAML::Node& input) {
    if ( input["row"] ) {
        this->row = input["row"].as<unsigned int>();
    }
    else if ( input["r"] ) {
        this->row = input["r"].as<unsigned int>();
    }
    else
        throw with_trace<std::domain_error>(boost::format("%1%: input has no row ('r'/'row') : %2%") % __func__% input);

    if ( input["column"] ) {
        this->column = input["column"].as<std::string>().at(0);
    }
    else if ( input["c"] ) {
        this->column = input["c"].as<std::string>().at(0);
    }
    else
        throw with_trace<std::domain_error>(boost::format("%1%: input has no column ('c'/'column') : %2%") % __func__% input);
}

std::ostream& seat_designation::dump(std::ostream& output) const {
    return output << boost::format("r%1$03d:c%2%") % this->row % this->column;
}

YAML::Emitter& seat_designation::dump(YAML::Emitter& output) const {
    output << boost::str(boost::format("r%1$03d:c%2%") % this->row % this->column);
    return output;
}

namespace airplane {

const std::map< char, object_type > char_object_type_map({
    { 'I', (+object_type::aisle) },
    { 'K', (+object_type::baby_station) },
    { 'B', (+object_type::bulkhead) },
    { 'C', (+object_type::closet) },
    { 'P', (+object_type::cockpit) },
    { 'G', (+object_type::galley) },
    { 'L', (+object_type::lavatory) },
    { '0', (+object_type::nullo) },
    { 'W', (+object_type::walkway) },
    { 'S', (+object_type::seat) },
    { 'X', (+object_type::door) },
    { 'Z', (+object_type::door_emergency) },
    { '|', (+object_type::window) },
    { '=', (+object_type::hull) },
});

const std::map< object_type, std::set< object_type_trait > > object_type_traits_map{
    { (+object_type::aisle), {
        object_type_trait::lateral_segment_barrier,
        object_type_trait::provides_legroom,
        object_type_trait::classless } },

    { (+object_type::baby_station), {
        object_type_trait::lateral_segment_item } },
    { (+object_type::bulkhead), {
        object_type_trait::lateral_segment_item,
        object_type_trait::provides_legroom } },
    { (+object_type::closet), {
        object_type_trait::lateral_segment_item,
        object_type_trait::consider_closeness } },
    { (+object_type::cockpit), {
        object_type_trait::lateral_segment_item,
        object_type_trait::classless } },
    { (+object_type::galley), {
        object_type_trait::lateral_segment_item,
        object_type_trait::consider_closeness } },
    { (+object_type::lavatory), {
        object_type_trait::lateral_segment_item,
        object_type_trait::consider_closeness } },
    { (+object_type::nullo), {
        object_type_trait::lateral_segment_item } },
    { (+object_type::walkway), {
        object_type_trait::lateral_segment_item,
        object_type_trait::provides_legroom } },

    { (+object_type::seat), {
        object_type_trait::lateral_segment_item,
        object_type_trait::specialized } },

    { (+object_type::window), {
        object_type_trait::airplane_boundary } },
    { (+object_type::hull), {
        object_type_trait::airplane_boundary } },
    { (+object_type::door), {
        object_type_trait::airplane_boundary,
        object_type_trait::consider_closeness,
        object_type_trait::classless } },
    { (+object_type::door_emergency), {
        object_type_trait::airplane_boundary,
        object_type_trait::consider_closeness,
        object_type_trait::classless } },
};

object::object(const class row& row, size_t index, const object_type& type, std::string classing)
    : row(row)
    , type(type)
    , index(index)
    , classing(std::move(classing)) {}

object::object(const class row& row, size_t index, const std::string& type, std::string classing)
    : object( row, index, object::type_from( type ), classing ) {}

object::object(const class row& row, size_t index, char type, std::string classing)
    : object( row, index, object::type_from( type ), classing ) {}

boost::optional<const object&> object::get_left() const {
    if (this->index == 0)
        return boost::optional<const object&>();
    else {
        const auto& ref = *this->row.get_objects().at(this->index - 1);
        if ( ref.get_type() == (+object_type::nullo) )
            return ref.get_left();
        else
            return boost::optional<const object&>(ref);
    }
}

boost::optional<const object&> object::get_right() const {
    if (this->index >= (this->row.get_objects().size() - 1))
        return boost::optional<const object&>();
    else {
        const auto& ref = *this->row.get_objects().at(this->index + 1);
        if ( ref.get_type() == (+object_type::nullo) )
            return ref.get_right();
        else
            return boost::optional<const object&>(ref);
    }
}

object_type object::type_from(const std::string& input) {
    if (input.size() != 1)
        throw with_trace<std::domain_error>(boost::format("%1%: input [%2%] length != 1") % __func__% input);
    return object::type_from(input.at(0));
}

object_type object::type_from(char input) {
    auto cit = char_object_type_map.find(input);
    if ( cit != char_object_type_map.end() ) {
        return cit->second;
    }
    throw with_trace<std::domain_error>(boost::format("%1%: input [%2%] has no associated object_type") % __func__% input);
}

const object_type_traits& object::traits() const {
    return object::traits( this->type );
}

bool object::has_trait(const object_type_trait& trait) const {
    const auto& traits = this->traits();
    return traits.count( trait ) != 0u;
}

const object_type_traits& object::traits(const object_type& type) {
    return object_type_traits_map.at( type );
}

bool object::has_trait(const object_type& type, const object_type_trait& trait) {
    const auto& traits = object::traits( type );
    return traits.count( trait ) != 0u;
}

YAML::Emitter& object::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    this->dump_members(output);
    output << YAML::EndMap;
    return output;
}

YAML::Emitter& object::dump_members(YAML::Emitter& output) const {
    output << YAML::Key << "r" << YAML::Value << this->row.get_index();
    output << YAML::Key << "c" << YAML::Value << this->index;
    output << YAML::Key << "type" << YAML::Value << this->get_type()._to_string();
    //output << YAML::Key << "width" << YAML::Value << dump::mydump(this->width);
    output << YAML::Key << "classing" << YAML::Value << dump::mydump(this->classing);
    return output;
}

std::ostream& object::dump(std::ostream& output) const {
    return output << boost::format("%1%{ %2% }") % this->get_type()._to_string() % dump::mxdetails(*this);
}

std::ostream& object::dump_details(std::ostream& output) const {
    //return output << boost::format( "width = %1%" ) % this->width;
    return output;
}

lateral_segment_object::lateral_segment_object( const class row& row, size_t index, const object_type& type, const class lateral_segment& lateral_segment, std::string classing )
    : object( row, index, type, classing )
    , lateral_segment( lateral_segment )
{
}

YAML::Emitter& lateral_segment_object::dump_members(YAML::Emitter& output) const {
    this->object::dump_members(output);
    output << YAML::Key << "lsi" << YAML::Value << this->lateral_segment.get_index();
    return output;
}

seat::seat(const class row& row, size_t index, const class lateral_segment& lateral_segment, const std::string& classing, seat_designation&& designation)
    : object(row, index, (+object_type::seat), classing)
    , lateral_segment_object(row, index, (+object_type::seat), lateral_segment, classing)
    , designation(std::move(designation)) {
}

boost::optional<const object&> seat::get_up() const {
    auto row_up = this->row.get_up();
    if ( !row_up ) return boost::optional<const object&>();
    return boost::optional<const object&>(*(row_up->get_objects().at(this->index)));
}

YAML::Emitter& seat::dump_members(YAML::Emitter& output) const {
    this->lateral_segment_object::dump_members(output);
    output << YAML::Key << "designation" << YAML::Value << this->designation;
    //output << YAML::Key << "classing" << YAML::Value << dump::mydump( this->classing );
    return output;
}

std::ostream& seat::dump_details(std::ostream& output) const {
    this->object::dump_details(output);
    return output << boost::format(", designation = %1%, classing = %2%") //
        % this->designation % this->classing;
}

lateral_segment::lateral_segment( const airplane::row& row, size_t index, const std::string& classing )
    : row( row )
    , index( index )
    , classing( classing )
{
}

YAML::Emitter& lateral_segment::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "index" << YAML::Value << index;
    output << YAML::Key << "classing" << YAML::Value << dump::mydump(classing);
    output << YAML::Key << "objects" << YAML::Value << YAML::BeginSeq;
    for (const auto& object : objects) {
        output << YAML::DoubleQuoted << YAML::Flow;
        output << object;
    }
    output << YAML::EndSeq;
    output << YAML::EndMap;
    return output;
}

boost::optional<const lateral_segment&> lateral_segment::get_left() const {
    if (this->index == 0)
        return boost::optional<const lateral_segment&>();
    else {
        const auto& ref = *this->row.get_lateral_segments().at(this->index - 1);
        return boost::optional<const lateral_segment&>(ref);
    }
}

boost::optional<const lateral_segment&> lateral_segment::get_right() const {
    if (this->index >= (this->row.get_lateral_segments().size() - 1))
        return boost::optional<const lateral_segment&>();
    else {
        const auto& ref = *this->row.get_lateral_segments().at(this->index + 1);
        return boost::optional<const lateral_segment&>(ref);
    }
}

row::row(class layout& layout, const row_context& context, const std::string& line, const std::smatch& match)
    : layout(layout)
    , index(context.index)
    , width(*(context.width))
    , classing(*context.classing) {
    auto content = match.str(2);

    auto designation_string = match.str(5);
    if (!designation_string.empty()) {
        if (designation_string == "+") {
            if (!context.last_designation) throw with_trace<std::domain_error>(boost::format("Last designation not defined for row with '+' designation %1%") % line);
            this->designation = (*(context.last_designation) + 1);
        }
        else
            this->designation = std::stoul(designation_string);
    }

    size_t object_index = 0;

    this->add_object(new airplane::object(*this, this->objects.size(), match.str(1), "*"));
    this->lateral_segments.emplace_back( new lateral_segment( *this, this->lateral_segments.size(), classing ) );

    auto parts = ascii::point::parse_line(content);

    size_t seat_column = 0;

    for (const auto& part : parts) {

        auto object_type = object::type_from(part.get_value());
        const auto& object_type_traits = object::traits( object_type );

        if ( object_type_traits.count( object_type_trait::lateral_segment_barrier ) ) {
            this->lateral_segments.emplace_back( new lateral_segment( *this, this->lateral_segments.size(), classing ) );
        }

        if ( object_type_traits.count( object_type_trait::specialized ) ) {
            switch (object_type) {
            case object_type::seat: {
                if (!this->designation) throw with_trace<std::domain_error>(boost::format("line '%1%' has no row designation") % line);
                char column_designation = (*context.letters).at(seat_column);
                std::unique_ptr<airplane::seat> seat_ptr(
                    new airplane::seat(*this, this->objects.size(), *(this->lateral_segments.back()), classing,
                        model::seat_designation(*this->designation, column_designation)));
                auto emplace_result = layout.mod_seat_map().emplace(seat_ptr->get_designation(), std::cref(*seat_ptr));
                if (!emplace_result.second) throw with_trace<std::logic_error>(boost::format("%1%: duplicate designation %2%") % __func__ % emplace_result.first->first);
                this->add_object(std::move(seat_ptr));
                seat_column += 1;
            } break;
            default:
                throw with_trace<std::logic_error>(boost::format("%1%: specialized type %2% not handled") % __func__ % object_type);
                break;
            }
        }
        else {
            if ( object_type_traits.count( object_type_trait::lateral_segment_item ) ) {
                this->add_object(new lateral_segment_object( *this, this->objects.size(), object_type, *(this->lateral_segments.back()), classing ));
            }
            else {
                this->add_object(new object( *this, this->objects.size(), object_type, classing ));
            }
        }

        if ( object_type_traits.count( object_type_trait::lateral_segment_item ) ) {
            this->lateral_segments.back()->mod_objects().emplace_back(std::cref(*(this->objects.back().get())));
        }


        if (context.letters && seat_column > context.letters->size())
            throw with_trace<std::domain_error>(boost::format("letter count %1% and seat count %2% mismatch") % context.letters->size() % seat_column);

    }

    this->add_object(new airplane::object(*this, this->objects.size(), match.str(3), "*"));

    if ( this->get_up() && this->has( object_type::seat ) && this->get_up()->get_objects().size() != this->objects.size() ) {
        throw with_trace<std::domain_error>(boost::format("Row with seats must have same amount of objects as precing row"));
    }

}

bool row::has(const object_type& type) const {
    return this->object_type_map.count( type ) != 0u;
}


boost::optional<const row&> row::get_up() const {
    if (this->index == 0)
        return boost::optional<const row&>();
    else
        return boost::optional<const row&>(*this->layout.get_rows().at(this->index - 1));
}

boost::optional<const row&> row::get_down() const {
    if (this->index >= (this->layout.get_rows().size() - 1))
        return boost::optional<const row&>();
    else
        return boost::optional<const row&>(*this->layout.get_rows().at(this->index + 1));
}

std::ostream& row::dump(std::ostream& output) const {
    return output << boost::format("row{ index = %1%, width = %2%, designation = %3%, objects = %4% }") //
        % this->index % this->width % this->designation % debug::mxdump(this->objects);
}

YAML::Emitter& row::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "index" << YAML::Value << index;
    output << YAML::Key << "width" << YAML::Value << width;
    output << YAML::Key << "designation" << YAML::Value << dump::mydump(designation);
    output << YAML::Key << "classing" << YAML::Value << dump::mydump(classing);
    output << YAML::Key << "objects" << YAML::Value << YAML::BeginSeq;
    for (const auto& object : objects) {
        output << YAML::DoubleQuoted << YAML::Flow;
        output << object;
    }
    output << YAML::EndSeq;
    output << YAML::Key << "lateral_segments" << YAML::Value << YAML::BeginSeq;
    for (const auto& lateral_segment : lateral_segments) {
        output << YAML::DoubleQuoted << YAML::Flow;
        output << lateral_segment;
    }
    output << YAML::EndSeq;
    output << YAML::EndMap;
    return output;
}

layout::layout(const std::string& input) {
    int line_index = -1;

    std::istringstream isstream(input);
    std::string line;
    row_context row_context;
    while (std::getline(isstream, line)) {
        line_index += 1;

        std::smatch match;
        if (std::regex_match(line, match, pattern::normal)) {
            if (!row_context.classing) throw with_trace<std::domain_error>(boost::format("Classing not defined at %1%") % line);
            this->rows.emplace_back(new row{*this, row_context, line, match});
            const auto& back = *(this->rows.back());
            row_context.last_designation = back.get_designation();
            if (back.has(object_type::seat)) row_context.next_seat_row++;
            row_context.index++;
        }
        else if (std::regex_match(line, match, pattern::indexing)) {
            auto indexing = match.str(1);
            auto parts = ascii::point::parse_line(indexing);
            boost::optional<std::vector<char> > letters = std::vector<char>();
            for (const auto& part : parts) {
                letters->push_back(part.get_value());
            }
            row_context.letters = letters;
        }
        else if (std::regex_match(line, match, pattern::meta)) {
            std::string key = match.str(1);
            std::string value = match.str(2);
            if (key == "class")
                row_context.classing = value;
            else
                throw with_trace<std::domain_error>(boost::format("Unknown meta line %1%") % line);
        }
        else if (std::regex_match(line, match, pattern::blank)) {
            // Nothing
        }
        else {
            throw with_trace<std::domain_error>(boost::format("Malformed line %1% : %2%") % (line_index + 1) % line);
        }
    }
}

YAML::Emitter& layout::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "rows" << YAML::Value << YAML::BeginSeq;
    for (const auto& row : rows) {
        output << row;
    }
    output << YAML::EndSeq;
    output << YAML::EndMap;
    return output;
}

std::ostream& layout::dump(std::ostream& output) const {
    return output << boost::format("layout{ rows = %1% }") //
        % debug::mxdump(this->rows);
}

std::unique_ptr<layout> layout::from_file(const std::string& filename) {
    const auto root = YAML::LoadFile(filename);
    const auto layout_ascii = root["ascii"].as<std::string>();
    return std::unique_ptr<layout>(new model::airplane::layout(layout_ascii));
}

} // namespace airplane

namespace attributes {

YAML::Emitter& property::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "key" << YAML::Value << this->get_key();
    output << YAML::Key << "value" << YAML::Value << this->get_value();
    output << YAML::EndMap;
    return output;
}

std::string property_lateral_adjacent::key{ (+property_types::lateral_adjacent)._to_string() };
std::set<std::string> property_lateral_adjacent::allowed_values{
    (+airplane::object_type::aisle)._to_string(),
    (+airplane::object_type::seat)._to_string(),
    (+airplane::object_type::window)._to_string(),
};

property_lateral_adjacent::property_lateral_adjacent(const std::string& value)
    : property(value) { }

property_lateral_adjacent::property_lateral_adjacent(const airplane::seat& seat) {
    const auto& left_type = seat.get_left()->get_type();
    const auto& right_type = seat.get_right()->get_type();
    if ( left_type == (+airplane::object_type::window) || right_type == (+airplane::object_type::window) ) {
        this->value = (+airplane::object_type::window)._to_string();
    }
    else if ( left_type == (+airplane::object_type::aisle) || right_type == (+airplane::object_type::aisle) ) {
        this->value = (+airplane::object_type::aisle)._to_string();
    }
    else {
        this->value = (+airplane::object_type::seat)._to_string();
    }
    if ( this->allowed_values.find( this->value ) == this->allowed_values.end() ) {
        throw with_trace<std::domain_error>(boost::format("%1% calculated wrong value %2%") % this->key % this->value );
    }
}

std::string property_lateral_side::key{ (+property_types::lateral_side)._to_string() };
std::set<std::string> property_lateral_side::allowed_values{
    "left",
    "middle",
    "right",
};

property_lateral_side::property_lateral_side(const std::string& value)
    : property(value) { }

property_lateral_side::property_lateral_side(const airplane::seat& seat) {
    if ( !seat.get_lateral_segment().get_left() ) {
        this->value = "left";
    }
    else if ( !seat.get_lateral_segment().get_right() ) {
        this->value = "right";
    }
    else {
        this->value = "middle";
    }
    if ( this->allowed_values.find( this->value ) == this->allowed_values.end() ) {
        throw with_trace<std::domain_error>(boost::format("%1% calculated wrong value %2%") % this->key % this->value );
    }
}

std::string property_legroom::key{ (+property_types::legroom)._to_string() };
std::set<std::string> property_legroom::allowed_values{
    "extra",
    "normal",
};

property_legroom::property_legroom(const std::string& value)
    : property(value) { }

property_legroom::property_legroom(const airplane::seat& seat) {
    auto object_up = seat.get_up();
    if ( object_up && object_up->has_trait( airplane::object_type_trait::provides_legroom ) ) {
        this->value = "extra";
    }
    else {
        this->value = "normal";
    }
    if ( this->allowed_values.find( this->value ) == this->allowed_values.end() ) {
        throw with_trace<std::domain_error>(boost::format("%1% calculated wrong value %2%") % this->key % this->value );
    }
}

YAML::Emitter& classing::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "property_seat_map" << YAML::Value << this->property_seat_map;
    output << YAML::Key << "object_close_seat_map" << YAML::Value << this->object_close_seat_map;
    output << YAML::EndMap;
    return output;

}

layout::layout(const airplane::layout& layout) {
    const auto& rows = layout.get_rows();

    if ( rows.size() == 0u )
        throw with_trace<std::domain_error>(boost::format("%1%: no rows to operate on") % __func__ );

    std::set< airplane::object_type > closeness_types;

    for (const auto& type : airplane::object_type::_values()) {
        if ( !airplane::object::has_trait( type, airplane::object_type_trait::consider_closeness ) ) continue;
        closeness_types.insert( type );
    }

    for (const auto& row_ptr : rows) {
        const auto& row = *row_ptr;
        row_to_object_close[row.get_index()];
        auto max_index = rows.size() - 1;
        auto max_distance = std::max( row.get_index(), max_index - row.get_index() ) + 1;
        for ( auto type : closeness_types ) {
            boost::optional< size_t > found_distance;
            for ( size_t distance = 0; distance < max_distance; ++distance ) {
                const airplane::row* up_ptr = nullptr;
                const airplane::row* down_ptr = nullptr;
                std::vector< const airplane::row* > search_ptrs;
                //if ( index_up > 0 ) {
                if ( row.get_index() >= ( 0 + distance ) ) {
                    auto index_up = row.get_index() - distance;
                    up_ptr = rows.at( index_up ).get();
                    search_ptrs.push_back( up_ptr );
                }
                auto index_down = row.get_index() + distance;
                if ( index_down <= max_index ) {
                    down_ptr = rows.at( index_down ).get();
                    search_ptrs.push_back( down_ptr );
                }
                for ( const airplane::row* row_against_ptr : search_ptrs ) {
                    const airplane::row& row_against = *row_against_ptr;
                    if ( !airplane::object::has_trait( type, airplane::object_type_trait::classless ) && row.get_classing() != row_against.get_classing() ) continue;
                    if ( !row_against.has( type ) ) continue;
                    found_distance = distance;
                    break;
                }
                if ( found_distance ) break;
            }
            if ( found_distance ) {
                auto distance = *found_distance;
                row_to_object_distance[row.get_index()][type] = distance;
                if ( distance <= 10 ) {
                    row_to_object_close[row.get_index()].insert( type );
                }
            }
        }
    }

    for (const auto& item : layout.get_seat_map()) {

        const auto& designation = item.first;
        const airplane::seat& seat = item.second;
        auto classing = seat.get_classing();
        auto& classing_attributes = classing_map[classing];
        classing_attributes.mod_available_seats().emplace( designation );
        {
            this->add_property(seat, new property_lateral_adjacent(seat));
        }
        {
            this->add_property(seat, new property_lateral_side(seat));
        }
        {
            this->add_property(seat, new property_legroom(seat));
        }
        {
            auto result = seat_close_objects.emplace(designation, std::cref(row_to_object_close.at(seat.get_row().get_index())));
            for ( const airplane::object_type& type : result.first->second.get() ) {
                classing_attributes.mod_object_close_seat_map()[type].emplace(designation);
            }
        }
    };
}

void layout::remove_seat(const std::string& classing, const seat_designation& seat) {
    auto& classing_attributes = classing_map[classing];
    if ( seat_properties.count(seat) )
    {
        const auto& properties = seat_properties.at(seat);
        for ( const auto& property_ptr : properties ) {
            const auto& property = *property_ptr;
            classing_attributes.mod_property_seat_map()[property].erase( seat );
        }
        //seat_properties.erase(seat);
    }
    if ( seat_close_objects.count(seat) )
    {
        const auto& close_objects = seat_close_objects.at(seat);
        for ( const auto& close_object : close_objects.get() ) {
            classing_attributes.mod_object_close_seat_map()[close_object].erase( seat );
        }
        //seat_close_objects.erase(seat);
    }
    classing_attributes.mod_available_seats().erase(seat);

    //seat_close_objects.erase(seat);
}

YAML::Emitter& layout::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "seat_properties" << YAML::Value << this->seat_properties;
    output << YAML::Key << "seat_close_objects" << YAML::Value << dump::mydump( this->seat_close_objects );
    output << YAML::Key << "row_to_object_distance" << YAML::Value << this->row_to_object_distance;
    output << YAML::Key << "row_to_object_close" << YAML::Value << this->row_to_object_close;
    output << YAML::Key << "classing_map" << YAML::Value << this->classing_map;
    output << YAML::EndMap;
    return output;
}


} // namespace attributes

namespace passanger {

preference::preference(size_t index, const YAML::Node& input)
    : index(index) {
}

YAML::Emitter& preference::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    this->dump_members(output);
    output << YAML::EndMap;
    return output;
}

YAML::Emitter& preference::dump_members(YAML::Emitter& output) const {
    output << YAML::Key << "index" << YAML::Value << this->index;
    return output;
}

std::unique_ptr<preference> preference::create(size_t index, const YAML::Node& input) {
    if (input[(+preference_type::match)._to_string()] != nullptr) {
        return std::unique_ptr<preference>(new preference_match(index, input));
    }
    else if (input[(+preference_type::close_to)._to_string()] != nullptr) {
        return std::unique_ptr<preference>(new preference_close_to(index, input));
    }
    else {
        throw with_trace<std::domain_error>(boost::format("preference::create: could not process node %1%") % input);
    }
}

preference_match::preference_match(size_t index, const YAML::Node& input)
    : preference(index, input) {
    const auto key = input[(+preference_type::match)._to_string()].as<std::string>();
    const auto value = input["value"].as<std::string>();
    if ( key == "legroom" ) {
       this->property.reset( new attributes::property_legroom( value ) );
    }
    else if ( key == "lateral_side" ) {
       this->property.reset(  new attributes::property_lateral_side( value ) );
    }
    else if ( key == "lateral_adjacent" ) {
       this->property.reset( new attributes::property_lateral_adjacent( value ) );
    } else {
        throw with_trace<std::domain_error>(boost::format("%1%: invalid match preference with key = %1% and value %2%") % __func__ % key % value);
    }
}

YAML::Emitter& preference_match::dump_members(YAML::Emitter& output) const {
    this->preference::dump_members(output);
    output << YAML::Key << (+preference_type::match)._to_string() << YAML::Value << this->property->get_key();
    output << YAML::Key << "value" << YAML::Value << this->property->get_value();
    return output;
}

preference_close_to::preference_close_to(size_t index, const YAML::Node& input)
    : preference(index, input) {
    const auto& string = input[(+preference_type::close_to)._to_string()].as<std::string>();
    this->object_type = airplane::object_type::_from_string( string.c_str() );
}

YAML::Emitter& preference_close_to::dump_members(YAML::Emitter& output) const {
    this->preference::dump_members(output);
    output << YAML::Key << (+preference_type::close_to)._to_string() << YAML::Value << this->object_type;
    return output;
}


manifest_passanger::manifest_passanger(size_t index, const YAML::Node& input) {
    if ( input["id"] == nullptr ) throw with_trace<std::domain_error>(boost::format("%1%: no id for passanger %2% : %3%") % __func__ % index % input );
    this->id = input["id"].as<std::string>();
    if (this->id == "<<AUTO>>") this->id = boost::str(boost::format("AUTO-%1$04d") % index);

    if ( input["seat"] )  {
        this->seat = seat_designation( input["seat"] );
    } else {
        if ( input["class"] == nullptr ) throw with_trace<std::domain_error>(boost::format("%1%: no class for passanger %2% : %3%") % __func__ % index % input );
        this->classing = input["class"].as<std::string>();
        if ( input["preferences"] ) {
            const auto& preferences_node = input["preferences"];
            for (const auto& preference_node : preferences_node) {
                this->preferences.emplace_back(preference::create(this->preferences.size(), preference_node));
            }
        }
    }
}

YAML::Emitter& manifest_passanger::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "id" << YAML::Value << this->id;
    output << YAML::Key << "seat" << YAML::Value << dump::mydump( this->seat );
    output << YAML::Key << "class" << YAML::Value << dump::mydump( this->classing );
    output << YAML::Key << "preferences" << YAML::Value << this->preferences;
    output << YAML::EndMap;
    return output;
}

manifest::manifest(const YAML::Node& input) {
    const auto& passangers = input["passangers"];
    for (const auto& passanger_node : passangers) {
        this->passangers.emplace_back(new manifest_passanger(this->passangers.size(), passanger_node));
    }
}

YAML::Emitter& manifest::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "passangers" << YAML::Value << this->passangers;
    output << YAML::EndMap;
    return output;
}

std::unique_ptr<manifest> manifest::from_file(const std::string& filename) {
    const auto root = YAML::LoadFile(filename);
    return std::unique_ptr<manifest>(new manifest(root));
}

} // namespace passanger


assignments::assignments(const airplane::layout& airplane_layout, const passanger::manifest& passanger_manifest, attributes::layout& attributes_layout)
    :
        airplane_layout( airplane_layout ),
        passanger_manifest( passanger_manifest ),
        attributes_layout( attributes_layout )
{
    this->assign();
}

void assignments::assign() {
    const auto& seat_map = this->airplane_layout.get_seat_map();
    size_t next_index = 0;
    for ( const auto& passanger_ptr : this->passanger_manifest.get_passangers() ) {
        size_t index = next_index++;
        const auto& passanger = *passanger_ptr;
        if ( passanger.get_seat() ) {
            const auto& seat = passanger.get_seat().get();
            if ( seat_map.count( seat ) == 0u ) {
                this->reject( passanger.get_id(), boost::format( "seat %1% does not exist in airplane layout" ) % seat );
                continue;
            }
            const std::string& classing = seat_map.at( seat ).get().get_classing();
            this->assign_seat_or_reject( classing, seat, passanger.get_id() );
            continue;
        }
        else if ( passanger.get_preferences().size() ) {
            const auto& classing_map = attributes_layout.get_classing_map();
            const auto& classing = *passanger.get_classing();
            if ( classing_map.count( classing ) == 0u ) {
                this->reject( passanger.get_id(), "Could not find any seats with class : " + classing );
                continue;
            }
            const auto& classing_attributes = classing_map.at( classing );
            const auto& property_seat_map = classing_attributes.get_property_seat_map();
            const auto& object_close_seat_map = classing_attributes.get_object_close_seat_map();
            std::set<seat_designation> options;
            for ( const auto& preference_ptr : passanger.get_preferences() ) {
                const auto& preference = *preference_ptr;
                boost::optional<const std::set<seat_designation>&> more_options_opt;
                if ( preference.type() == (+passanger::preference_type::match) ) {
                    const auto& match = dynamic_cast< const passanger::preference_match& >(preference);
                    const auto& property = match.get_property();
                    if ( property_seat_map.count( property ) != 0u ) {
                        more_options_opt = property_seat_map.at( property );
                    }
                }
                else if ( preference.type() == (+passanger::preference_type::close_to) ) {
                    const auto& close_to = dynamic_cast< const passanger::preference_close_to& >(preference);
                    const auto& object_type = close_to.get_object_type();
                    if ( object_close_seat_map.count( object_type ) != 0u ) {
                        more_options_opt = object_close_seat_map.at( object_type );
                    }
                }
                else {
                    throw with_trace<std::domain_error>(boost::format("%1%: unhandled preference type in passanger manifest %2%") % __func__ % preference.type());
                }
                if ( more_options_opt ) {
                    const auto& more_options = *more_options_opt;
                    if ( options.size() == 0u )
                        options.insert( more_options.begin(), more_options.end() );
                    else {
                        std::set<seat_designation> new_options;
                        std::set_intersection( options.begin(), options.end(), more_options.begin(), more_options.end(), std::inserter( new_options, new_options.begin() ) );
                        if ( new_options.size() != 0u ) options = new_options;
                    }
                }
            }
            if ( options.size() != 0u ) {
                this->assign_seat( classing, *options.begin(), passanger.get_id() );
                continue;
            }
        }
        // nothing assgined yet, just take any seat ...
        const auto& classing_map = attributes_layout.get_classing_map();
        const auto& classing = *passanger.get_classing();
        if ( classing_map.count( classing ) == 0u ) {
            this->reject( passanger.get_id(), boost::format( "No seat with class %1%" ) % classing );
            continue;
        }
        const auto& classing_attributes = classing_map.at( classing );
        const auto& available_seat = classing_attributes.get_available_seats();
        if ( available_seat.size() == 0u ) {
            this->reject( passanger.get_id(), boost::format( "No more seats available in class %1%" ) % classing );
            continue;
        }
        else {
            this->assign_seat( classing, *available_seat.begin(), passanger.get_id() );
        }
    }
}

void assignments::assign_seat(const std::string& classing, const seat_designation& seat, const std::string& passanger_id) {
    auto result = this->try_assign_seat( classing, seat, passanger_id );
    if ( !result.first ) throw with_trace<std::domain_error>(result.second);
}

std::pair< bool, std::string > assignments::try_assign_seat(const std::string& classing, const seat_designation& seat, const std::string& passanger_id) {
    if ( this->seat_passanger_id_map.count( seat ) != 0u )
        return std::make_pair( false, boost::str(boost::format("seat %1% already assgined to passanger %2%") % seat % this->seat_passanger_id_map.at(seat) ) );
    if ( this->passanger_id_seat_map.count( passanger_id ) != 0u )
        return std::make_pair(false, boost::str(boost::format("passanger_id %1% already has seat %2% assigned") % passanger_id % this->passanger_id_seat_map.at(passanger_id)));
    this->seat_passanger_id_map.emplace(seat, passanger_id);
    this->passanger_id_seat_map.emplace(passanger_id, seat);
    this->attributes_layout.remove_seat( classing, seat );
    return std::make_pair( true, "okay" );
}

void assignments::reject(const std::string& passanger_id, const std::string& reason) {
    this->passanger_rejections.emplace( passanger_id, reason );
}

void assignments::reject(const std::string& passanger_id, const boost::format& reason) {
    this->reject( passanger_id, boost::str( reason ) );
}


void assignments::assign_seat_or_reject(const std::string& classing, const seat_designation& seat, const std::string& passanger_id) {
    auto result = this->try_assign_seat( classing, seat, passanger_id );
    if ( !result.first ) this->passanger_rejections.emplace( passanger_id, result.second );
}

YAML::Emitter& assignments::dump(YAML::Emitter& output) const {
    output << YAML::BeginMap;
    output << YAML::Key << "assignments" << YAML::Value << this->passanger_id_seat_map;
    output << YAML::Key << "rejections" << YAML::Value << this->passanger_rejections;
    output << YAML::EndMap;
    return output;
}

} // namespace model
