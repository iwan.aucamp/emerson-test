#pragma once
#include "debug.hpp"
#include "enum.h"
#include "global.hpp"
#include "util.hpp"
#include <boost/format.hpp>
#include <boost/functional/hash.hpp>
#include <boost/geometry.hpp>
#include <boost/optional.hpp>
#include <boost/rational.hpp>
#include <cctype>
#include <regex>
#include <string>
#include <unordered_map>

namespace model {

namespace ascii {

class point
{
private:
    const char value;
    const size_t index;
    const size_t position;
public:
    point(char value, size_t index, size_t position);

    char get_value() const { return this->value; };
    size_t get_index() const { return this->index; };
    size_t get_position() const { return this->position; };


public:
    static std::vector<point> parse_line(const std::string& line);
};

std::ostream& operator<<(std::ostream& output, const point& input);

} // namespace ascii


namespace pattern {
extern std::regex normal;
extern std::regex indexin;
extern std::regex meta;
extern std::regex blank;
} // namespace pattern

class seat_designation
    : virtual public dump::ostream_dumpable,
      virtual public dump::yaml_dumpable
{
private:
    unsigned int row;
    char column;
public:
    seat_designation(const seat_designation& other) = default;
    seat_designation(seat_designation&& other) = default;
    seat_designation& operator=(seat_designation&& other) = default;
    seat_designation& operator=(const seat_designation& other) = default;

    seat_designation(unsigned int row, char column);
    seat_designation(const YAML::Node& input);

    unsigned int get_row() const { return this->row; }
    char get_column() const { return this->column; }

    std::size_t hash() const noexcept {
        std::size_t seed = 0;
        boost::hash_combine(seed, this->row);
        boost::hash_combine(seed, this->column);
        return seed;
    };
    bool operator<(const seat_designation& other) const {
        return std::tie(this->row, this->column) < std::tie(other.row, other.column);
    }
    bool operator==(const seat_designation& other) const {
        return std::tie(this->row, this->column) == std::tie(other.row, other.column);
    }
    std::ostream& dump(std::ostream& output) const override;
    YAML::Emitter& dump(YAML::Emitter& output) const override;
    ~seat_designation() override = default;
};

struct seat_designation_hasher
{
    std::size_t operator()(const seat_designation& object) const noexcept {
        return object.hash();
    }
};


namespace airplane {

BETTER_ENUM(object_type, unsigned int,
    aisle = 0u,
    baby_station,
    bulkhead,
    closet,
    cockpit,
    galley = 5u,
    lavatory,
    nullo,
    walkway,
    seat,
    window = 10u,
    hull,
    door,
    door_emergency)

extern const std::map<char, object_type> char_object_type_map;


BETTER_ENUM(object_type_trait, unsigned int,
    specialized = 0u,
    airplane_boundary,
    lateral_segment_barrier,
    lateral_segment_item,
    provides_legroom,
    consider_closeness,
    classless)

using object_type_traits = std::set< object_type_trait >;

extern const std::map< object_type, object_type_traits > object_type_traits_map;

class row;
class layout;
class lateral_segment;

class object
    : virtual public dump::ostream_dumpable,
      virtual public dump::yaml_dumpable
{
public:
    const airplane::row& row;
    const object_type type;
    const size_t index;
    const std::string classing;
public:
    object(const class row& row, size_t index, const object_type& type, std::string classing);
    object(const class row& row, size_t index, const std::string& type, std::string classing);
    object(const class row& row, size_t index, char type, std::string classing);

    const airplane::row& get_row() const { return this->row; }
    const object_type& get_type() const { return this->type; }
    const std::string& get_classing() const { return this->classing; };

    boost::optional<const object&> get_left() const;
    boost::optional<const object&> get_right() const;

    std::ostream& dump(std::ostream& output) const override;
    virtual std::ostream& dump_details(std::ostream& output) const;
    YAML::Emitter& dump(YAML::Emitter& output) const override;
    virtual YAML::Emitter& dump_members(YAML::Emitter& output) const;

    const object_type_traits& traits() const;
    bool has_trait(const object_type_trait& trait) const;

    static object_type type_from(const std::string& input);
    static object_type type_from(char input);

    static const object_type_traits& traits(const object_type& type);
    static bool has_trait(const object_type& type, const object_type_trait& trait);

    ~object() override = default;
};

class lateral_segment_object
    : virtual public object
{
private:
    const class lateral_segment& lateral_segment;
public:
    lateral_segment_object(const class row& row, size_t index, const object_type& object_type, const class lateral_segment& lateral_segment, std::string classing);
    const class lateral_segment& get_lateral_segment() const { return this->lateral_segment; }
    YAML::Emitter& dump_members(YAML::Emitter& output) const override;
};

class seat
    : virtual public lateral_segment_object
{
private:
    const seat_designation designation;
public:
    seat(const class row& row, size_t index, const class lateral_segment& lateral_segment, const std::string& classing, seat_designation&& designation);

    const seat_designation& get_designation() const { return this->designation; };
    boost::optional<const object&> get_up() const;

    std::ostream& dump_details(std::ostream& output) const override;
    YAML::Emitter& dump_members(YAML::Emitter& output) const override;

    ~seat() override = default;
};

using owned_objects = std::vector<std::unique_ptr<object>>;
using objects = std::vector<std::reference_wrapper<const object>>;

struct row_context
{
    boost::optional<unsigned int> last_designation = boost::optional<unsigned int>();
    boost::optional<std::string> classing = boost::optional<std::string>();
    boost::optional<size_t> width{0};
    size_t next_seat_row{0};
    size_t index{0};
    boost::optional<std::vector<char>> letters = boost::optional<std::vector<char>>();
};

class lateral_segment
    : virtual public dump::yaml_dumpable
{
private:
    const airplane::row& row;
    const size_t index = 0;
    const std::string classing;
    airplane::objects objects;
public:
    lateral_segment( const airplane::row& row, size_t index, const std::string& classing );
    YAML::Emitter& dump(YAML::Emitter& output) const override;

    const airplane::row& get_row() const { return this->row; }
    size_t get_index() const { return this->index; }
    const std::string& get_classing() const { return this->classing; }
    const airplane::objects& get_objects() const { return this->objects; }
    airplane::objects& mod_objects() { return this->objects; }

    boost::optional<const lateral_segment&> get_left() const;
    boost::optional<const lateral_segment&> get_right() const;
};

using owned_lateral_segments = std::vector<std::unique_ptr<lateral_segment>>;
using object_type_map = std::map< object_type, std::reference_wrapper< const object > >;

class row
    : virtual public dump::ostream_dumpable,
      virtual public dump::yaml_dumpable
{
private:
    const airplane::layout& layout;
    const size_t index{0};
    const size_t width{0};
    boost::optional<unsigned int> designation;
    const std::string classing;
    airplane::owned_objects objects;
    airplane::object_type_map object_type_map;
    airplane::owned_lateral_segments lateral_segments;
private:
    template<class... Args>
    const object& add_object(Args&&... args) {
        this->objects.emplace_back(std::forward<Args>(args)...);
        const auto& ref = *(this->objects.back());
        this->object_type_map.emplace(ref.get_type(), std::cref(ref));
        return ref;
    }
public:
    row(class layout& layout, const row_context& context, const std::string& input, const std::smatch& match);

    bool has(const object_type& type) const;

    boost::optional<const row&> get_up() const;
    boost::optional<const row&> get_down() const;


    size_t get_index() const { return this->index; }
    const boost::optional<unsigned int>& get_designation() const { return this->designation; }
    const std::string& get_classing() const { return this->classing; }
    const airplane::owned_objects& get_objects() const { return this->objects; }
    const airplane::object_type_map& get_object_type_map() const { return this->object_type_map; }
    const airplane::owned_lateral_segments& get_lateral_segments() const { return this->lateral_segments; }

    std::ostream& dump(std::ostream& output) const override;
    YAML::Emitter& dump(YAML::Emitter& output) const override;

    ~row() override = default;
};

using rows = std::vector<std::unique_ptr<row>>;
using seat_map = std::map<seat_designation, std::reference_wrapper<const seat>>;

class layout
    : virtual public dump::ostream_dumpable,
      virtual public dump::yaml_dumpable
{
private:
    airplane::rows rows;
    airplane::seat_map seat_map;
public:
    std::ostream& dump(std::ostream& output) const override;
    YAML::Emitter& dump(YAML::Emitter& output) const override;

    layout(const std::string& input);
    layout(layout&&) = default;
    layout& operator=(layout&&) = default;

    const airplane::rows& get_rows() const { return this->rows; };
    const airplane::seat_map& get_seat_map() const { return this->seat_map; };
    airplane::seat_map& mod_seat_map() { return this->seat_map; };

    static std::unique_ptr<layout> from_file(const std::string& filename);

    ~layout() override = default;
};

} // namespace airplane

namespace attributes {

class property
    : virtual public dump::yaml_dumpable
{
protected:
    std::string value;
    property() = default;
public:
    using this_type = property;
    virtual const std::string& get_key() const = 0;
    virtual const std::string& get_value() const { return this->value; }
    virtual const std::string& set_value(const std::string& value) { return this->value = value; }
    virtual const std::set< std::string >& get_allowed_values() const = 0;

    YAML::Emitter& dump(YAML::Emitter& output) const;

    bool operator<(const this_type& other) const {
        return std::tie(this->get_key(), this->get_value()) < std::tie(other.get_key(), other.get_value());
    }

    bool operator==(const this_type& other) const {
        return std::tie(this->get_key(), this->get_value()) == std::tie(other.get_key(), other.get_value());
    }

    property(const std::string& value) : value(value) {}

    std::size_t hash() const noexcept {
        std::size_t seed = 0;
        boost::hash_combine(seed, this->get_key());
        boost::hash_combine(seed, this->get_value());
        return seed;
    };

    virtual ~property() {
    };
};

BETTER_ENUM(property_types, unsigned int,
    lateral_adjacent = 0u,
    lateral_side,
    legroom
    )

class property_lateral_adjacent
    : virtual public property
{
private:
    using this_type = property_lateral_adjacent;
    static std::string key;
    static std::set<std::string> allowed_values;
public:
    const std::string& get_key() const override { return this_type::key; }
    const std::set< std::string >& get_allowed_values() const override { return this_type::allowed_values; }

    explicit property_lateral_adjacent(const std::string& value);
    explicit property_lateral_adjacent(const airplane::seat& seat);

    ~property_lateral_adjacent() override {
    }
};

class property_lateral_side
    : virtual public property
{
private:
    using this_type = property_lateral_side;
    static std::string key;
    static std::set<std::string> allowed_values;
public:
    const std::string& get_key() const override { return this_type::key; }
    const std::set< std::string >& get_allowed_values() const override { return this_type::allowed_values; }

    explicit property_lateral_side(const std::string& value);
    explicit property_lateral_side(const airplane::seat& seat);

    ~property_lateral_side() override {
    }
};

class property_legroom
    : virtual public property
{
private:
    using this_type = property_legroom;
    static std::string key;
    static std::set<std::string> allowed_values;
public:
    const std::string& get_key() const override { return this_type::key; }
    const std::set< std::string >& get_allowed_values() const override { return this_type::allowed_values; }

    explicit property_legroom(const std::string& value);
    explicit property_legroom(const airplane::seat& seat);

    ~property_legroom() override {
    }
};

//using owned_seat_properties = std::multimap<seat_designation, std::unique_ptr<property>>;
//using seat_close_objects = std::multimap<seat_designation, airplane::object_type>;
//using seat_close_objects = std::map<seat_designation, std::set<std::reference_wrapper<const object_type>, reference_wrapper_comparator<const object_type>>>;
//using seat_properties = std::map<seat_designation, std::set<airplane::object_type>>;
//using property_seat_map = std::multimap<std::reference_wrapper<const property>, seat_designation, reference_wrapper_comparator<const property>>;
//using object_close_seat_map = std::multimap<airplane::object_type, seat_designation>;
using property_seat_map = std::map<std::reference_wrapper<const property>, std::set<seat_designation>, reference_wrapper_comparator<const property>>;
using object_close_seat_map = std::map<airplane::object_type, std::set<seat_designation>>;
using available_seats = std::set<seat_designation>;

class classing
    : virtual public dump::yaml_dumpable
{
protected:
    attributes::property_seat_map property_seat_map;
    attributes::object_close_seat_map object_close_seat_map;
    attributes::available_seats available_seats;
public:
    const attributes::property_seat_map& get_property_seat_map() const { return this->property_seat_map; }
    attributes::property_seat_map& mod_property_seat_map() { return this->property_seat_map; }
    const attributes::object_close_seat_map& get_object_close_seat_map() const { return this->object_close_seat_map; }
    attributes::object_close_seat_map& mod_object_close_seat_map() { return this->object_close_seat_map; }
    const attributes::available_seats& get_available_seats() const { return this->available_seats; }
    attributes::available_seats& mod_available_seats() { return this->available_seats; }

    YAML::Emitter& dump(YAML::Emitter& output) const override;
    ~classing() override = default;
};

using classing_map = std::map< std::string, classing >;

using owned_seat_properties = std::map<seat_designation, std::set<std::unique_ptr<property>, unique_ptr_comparator<property>>>;
using seat_close_objects = std::map<seat_designation, std::reference_wrapper<const std::set<airplane::object_type>>>;

class layout
    : virtual public dump::yaml_dumpable
{
private:
    std::map<size_t,std::map<airplane::object_type,size_t>> row_to_object_distance;
    std::map<size_t,std::set<airplane::object_type>> row_to_object_close;
    attributes::owned_seat_properties seat_properties;
    attributes::seat_close_objects seat_close_objects;
    attributes::classing_map classing_map;
public:
    const attributes::classing_map& get_classing_map() const { return this->classing_map; }
    attributes::classing_map& mod_classing_map() { return this->classing_map; }
private:
    template<class... Args>
    const property& add_property(const airplane::seat& seat, property* property_ptr) {
        auto result = seat_properties[seat.get_designation()].emplace( property_ptr );
        const auto& ref =  *(*(result.first));
        auto& classing_attributes = classing_map[seat.get_classing()];
        //classing_attributes.mod_property_seat_map().emplace( std::cref(ref), seat.get_designation() );
        classing_attributes.mod_property_seat_map()[std::cref(ref)].emplace( seat.get_designation() );
        return ref;
    }
public:
    layout(const airplane::layout& layout);
    void remove_seat(const std::string& classing, const seat_designation& seat);
    YAML::Emitter& dump(YAML::Emitter& output) const override;
    ~layout() override = default;
};

} // namespace attributes

namespace passanger {

BETTER_ENUM(preference_type, unsigned int,
    match = 0u,
    close_to)

class preference
    : virtual public dump::yaml_dumpable
{
protected:
    size_t index;
public:
    preference(size_t index, const YAML::Node& input);
    virtual preference_type type() const = 0;
    ~preference() override = default;
    YAML::Emitter& dump(YAML::Emitter& output) const override;
    virtual YAML::Emitter& dump_members(YAML::Emitter& output) const = 0;
    static std::unique_ptr<preference> create(size_t index, const YAML::Node& input);
};

class preference_match
    : virtual public dump::yaml_dumpable,
      virtual public preference
{
private:
    //std::string property;
    //std::string value;
    std::unique_ptr<attributes::property> property;
public:
    const attributes::property& get_property() const { return *property; }
public:
    preference_match(size_t index, const YAML::Node& input);
    preference_type type() const override { return preference_type::match; };
    YAML::Emitter& dump_members(YAML::Emitter& output) const override;
    ~preference_match() override = default;
};

class preference_close_to
    : virtual public dump::yaml_dumpable,
      virtual public preference
{
private:
    airplane::object_type object_type = airplane::object_type::nullo;
public:
    const airplane::object_type& get_object_type() const { return this->object_type; }
public:
    preference_close_to(size_t index, const YAML::Node& input);
    preference_type type() const override { return preference_type::close_to; };
    YAML::Emitter& dump_members(YAML::Emitter& output) const override;
    ~preference_close_to() override = default;
};

using preferences = std::vector<std::unique_ptr<preference>>;

class manifest_passanger
    : virtual public dump::yaml_dumpable
{
private:
    std::string id;
    boost::optional< seat_designation > seat;
    boost::optional< std::string > classing;
    passanger::preferences preferences;
public:
    const std::string& get_id() const { return this->id; }
    const boost::optional< seat_designation >& get_seat() const { return this->seat; }
    const boost::optional< std::string >& get_classing() const { return this->classing; }
    const passanger::preferences& get_preferences() const { return this->preferences; }
public:
    manifest_passanger(size_t index, const YAML::Node& input);
    manifest_passanger(manifest_passanger&&) = default;
    manifest_passanger& operator=(manifest_passanger&&) = default;
    YAML::Emitter& dump(YAML::Emitter& output) const override;
};

using owned_manifest_passangers = std::vector<std::unique_ptr<manifest_passanger>>;

class manifest
    : virtual public dump::yaml_dumpable
{
private:
    owned_manifest_passangers passangers;
public:
    const owned_manifest_passangers& get_passangers() const { return this->passangers; }
public:
    explicit manifest(const YAML::Node& input);
    manifest(manifest&&) = default;
    manifest& operator=(manifest&&) = default;

    YAML::Emitter& dump(YAML::Emitter& output) const override;

    static std::unique_ptr<manifest> from_file(const std::string& filename);
};


} // namespace passanger

using seat_passanger_id_map = std::map< seat_designation, std::string >;
using passanger_id_seat_map = std::map< std::string, seat_designation >;
using passanger_rejections = std::map< std::string, std::string >;

class assignments
    : virtual public dump::yaml_dumpable
{
private:
    const airplane::layout& airplane_layout;
    const passanger::manifest& passanger_manifest;
    attributes::layout& attributes_layout;
private:
    model::seat_passanger_id_map seat_passanger_id_map;
    model::passanger_id_seat_map passanger_id_seat_map;
    void assign();
    model::passanger_rejections passanger_rejections;
    std::pair< bool, std::string > try_assign_seat(const std::string& classing, const seat_designation& seat, const std::string& passanger_id);
    void assign_seat(const std::string& classing, const seat_designation& seat, const std::string& passanger_id);
    void assign_seat_or_reject(const std::string& classing, const seat_designation& seat, const std::string& passanger_id);
    void reject(const std::string& passanger_id, const std::string& reason);
    void reject(const std::string& passanger_id, const boost::format& reason);
public:
    assignments(const airplane::layout& airplane_layout, const passanger::manifest& passanger_manifest, attributes::layout& attributes_layout);
    YAML::Emitter& dump(YAML::Emitter& output) const;
};

} // namespace model
