#include "util.hpp"

class util
{
};

namespace boost {
void assertion_failed(char const* expr, char const* function, char const* file, long line) {
    throw_with_trace(std::runtime_error(boost::str(boost::format("boost::assertion_failed: %1%:%2%: %3%: Assertion `%4%` failed.") //
        % file % line % function % expr)));
}
void assertion_failed_msg(char const* expr, char const* msg, char const* function, char const* file, long line) {
    throw_with_trace(std::runtime_error(boost::str(boost::format("boost::assertion_failed: %1%:%2%: %3%: Assertion `%4%` failed: %5%") //
        % file % line % function % expr % msg)));
}
} // namespace boost
