#pragma once

#include <yaml-cpp/yaml.h>

#include <boost/exception/all.hpp>
#include <boost/format.hpp>
//#include <boost/bimap.hpp>
#include <boost/optional.hpp>
#include <boost/rational.hpp>
#include <boost/stacktrace.hpp>

/*
#define DEFINE_PROPERTY( prefix, property_name, suffix ) \
	private: \
		prefix ## property ## suffix; \
	public: \
		const prefix& get_property_name() const; \
		prefix& mod_property_name(); \
	private: \
*/

namespace YAML {

template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::reference_wrapper<ValueType>& input) {
    output << input.get();
    return output;
}

template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::unique_ptr<ValueType>& input) {
    if (input)
        output << *input;
    else
        output << YAML::Null;
    return output;
}

template <typename KeyType, typename ValueType, typename X0>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::multimap<KeyType, ValueType, X0>& input) {
	output << YAML::BeginMap;
	for( auto it = input.begin(); it != input.end(); it = input.upper_bound(it->first))
	{
		output << YAML::Key << it->first << YAML::Value << YAML::BeginSeq;
		auto range = input.equal_range( it->first );
		for ( auto it = range.first; it != range.second; ++it ) {
			output << it->second;
		}
		output << YAML::EndSeq;
	}
	output << YAML::EndMap;
	return output;
}

template <typename KeyType, typename ValueType, typename X0>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::map<KeyType, ValueType, X0>& input) {
	output << YAML::BeginMap;
	for( auto it = input.begin(); it != input.end(); it = input.upper_bound(it->first))
	{
		output << YAML::Key << it->first << YAML::Value << it->second;
	}
	output << YAML::EndMap;
	return output;
}

template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const boost::rational<ValueType>& input) {
    //output << boost::str(boost::format("r(%1%/%2%)") % input.numerator() % input.denominator());
    output << YAML::BeginMap;
    output << YAML::Key << "n" << YAML::Value << input.numerator();
    output << YAML::Key << "d" << YAML::Value << input.denominator();
    output << YAML::EndMap;
    return output;
}

template <typename TypeA, typename TypeB>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::pair<TypeA, TypeB>& input) {
    output << YAML::BeginMap;
    output << YAML::Key << "first" << YAML::Value << input.first;
    output << YAML::Key << "second" << YAML::Value << input.second;
    output << YAML::EndMap;
    return output;
}

template <typename TypeA, typename TypeB, typename TypeC>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::set<TypeA, TypeB, TypeC>& input) {
	output << YAML::BeginSeq;
    for ( const auto& item : input ) {
        output << item;
    }
	output << YAML::EndSeq;
    return output;
}

template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const boost::optional<ValueType>& input) {
    if (input)
        output << *input;
    else
        output << YAML::Null;
    return output;
}

} // namespace YAML

namespace stdx {
template <typename KeyType, typename ValueType, typename KeyType2>
boost::optional<ValueType&> get_optional(const std::map<KeyType, ValueType>& input, const KeyType2& key) {
	auto it = input.find( key );
	if ( it == input.end() ) return boost::optional<ValueType&>();
	else return boost::optional<ValueType&>();
}
} // namespace stdx

template <typename KeyType, typename ValueType, typename UnaryFunction>
void for_each_unique_key(const std::multimap< KeyType, ValueType >& input, UnaryFunction function) {
	for( auto it = input.begin(); it != input.end(); it = input.upper_bound(it->first))
	{
		function( it->first );
	}
}

template <typename KeyType, typename ValueType, typename UnaryFunction>
void for_each_equal_key(const std::multimap< KeyType, ValueType >& input, const KeyType& key, UnaryFunction function) {
	auto range = input.equal_range( key );
	for ( auto it = range.first; it != range.second; ++it ) {
		function( it->second );
	}
}

template <typename RefType>
bool operator <(const std::reference_wrapper<RefType>& a, const std::reference_wrapper<RefType>& b) {
	return a.get() < b.get();
}

template <typename RefType>
struct reference_wrapper_comparator {
    bool operator()(const std::reference_wrapper<RefType>& a, const std::reference_wrapper<RefType>& b) const {
        return a.get() < b.get();
    }
};

template <typename TypeA>
struct unique_ptr_comparator {
    bool operator()(const std::unique_ptr<TypeA>& a, const std::unique_ptr<TypeA>& b) const {
        return *a < *b;
    }
};

using traced = boost::error_info<struct tag_stacktrace, boost::stacktrace::stacktrace>;

template <typename ExceptionType>
ExceptionType with_trace(const boost::format& message) {
    return ExceptionType(boost::str(message));
}

template <typename ExceptionType>
ExceptionType with_trace(const std::string& message) {
    return ExceptionType(message);
}

template <class E>
void throw_with_trace(const E& e) {
    throw boost::enable_error_info(e)
        << traced(boost::stacktrace::stacktrace());
}

namespace dump {

template <typename T>
std::string stringify(const T& in) {
    std::stringstream output;
    output << in;
    return output.str();
}

template <typename ValueType>
class ydump
{
public:
    const ValueType& value;
    explicit ydump(const ValueType& value)
        : value(value) {}
};

template <typename ValueType>
ydump<ValueType> mydump(const ValueType& value) {
    return ydump<ValueType>(value);
}

class yaml_dumpable
{
public:
    virtual YAML::Emitter& dump(YAML::Emitter& output) const = 0;
    virtual ~yaml_dumpable() = default;
};

inline YAML::Emitter& operator<<(YAML::Emitter& output, const yaml_dumpable& input) {
    return input.dump(output);
}

class ysdump
{
public:
    const yaml_dumpable& value;
    explicit ysdump(const yaml_dumpable& value)
        : value(value) {}
};

inline ysdump mysdump(const yaml_dumpable& value) {
    return ysdump(value);
}

inline std::ostream& operator<<(std::ostream& output, const ysdump& input) {
    YAML::Emitter emitter;
    emitter << YAML::DoubleQuoted << YAML::Flow;
    emitter << input.value;
    output << emitter.c_str();
    return output;
}

/*
template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const std::reference_wrapper<ValueType>& input) {
    output << input.get();
    return output;
}
*/

template <typename ValueType>
YAML::Emitter& operator<<(YAML::Emitter& output, const ydump<ValueType>& input) {
    output << input.value;
    return output;
}

class ostream_dumpable
{
public:
    virtual std::ostream& dump(std::ostream& output) const = 0;
    //virtual std::ostream& dump_details( std::ostream& output ) const = 0;
    virtual ~ostream_dumpable() = default;
    /*
    virtual const std::string& dump( std::string& output ) const = 0;
    virtual std::string dump() const = 0;
    static const std::string& adapt_ostream_to_string( const dumpable& dumpable, std::string& output ) {
        std::stringsteam output;
        dumpable.dump( output );
        output = output.str();
        return output;
    }

    static std::string adapt_ostream_to_string( const dumpable& dumpable ) {
        std::stringsteam output;
        dumpable.dump( output );
        return output.str();
    }

    static const std::ostream& adapt_string_to_ostream( const dumpable& dumpable, std::string& ostream ) {
        ostream()
    }
    */
};

inline std::ostream& operator<<(std::ostream& output, const ostream_dumpable& input) {
    return input.dump(output);
}

template <typename ValueType>
class xdetails
{
public:
    const ValueType& value;
    explicit xdetails(const ValueType& value)
        : value(value) {}
};

template <typename ValueType>
xdetails<ValueType> mxdetails(const ValueType& value) {
    return xdetails<ValueType>(value);
}

template <typename ValueType>
std::ostream& operator<<(std::ostream& output, const xdetails<ValueType>& input) {
    return input.value.dump_details(output);
}

} // namespace dump
